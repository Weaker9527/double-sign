package com.rfy.authserver.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rfy.JWT.JWTUtil;
import com.rfy.ResponseWrapper.R;
import com.rfy.ResponseWrapper.ResponseCode;
import com.rfy.authserver.BO.LoginBo;
import com.rfy.authserver.DAO.mapper.UserMapper;
import com.rfy.authserver.Model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping
public class LoginController {

    @Value("${JWT.secretKey:123456}")
    private String secretKey;

    private final UserMapper userMapper;

    @PostMapping("/login")
    public R login(@RequestBody LoginBo loginBo) {
        if(checkPassword(loginBo)){
            QueryWrapper<User> userWrapper = new QueryWrapper<>();
            userWrapper.eq("name", loginBo.getUserName());
            User user = userMapper.selectOne(userWrapper);
            String token = JWTUtil.generateToken(loginBo.getUserName(), user.getId(), secretKey);
            Map<String, String> resultMap = new HashMap<String, String>();
            resultMap.put("token", token);
            resultMap.put("userId", user.getId());
            resultMap.put("userName", user.getName());
            return R.success(resultMap);
        }else{
            return R.error()
                    .setCode(ResponseCode.LOGIN_ERROR.getCode())
                    .setMessage(ResponseCode.LOGIN_ERROR.getMessage());
        }
    }

    @PostMapping("/getUserName")
    public R getUserName(@RequestHeader("token") String token){
        JWTUtil.verifyToken(token, secretKey);
        String userName = JWTUtil.getUserName(token);
        return R.success(userName);
    }

    //校验密码的方法
    Boolean checkPassword(LoginBo loginBo){
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.eq("name", loginBo.getUserName());
        User user = userMapper.selectOne(userWrapper);
        if(null == user){
            return false;
        }
        //TODO: 此处以后需要添加密码加密
        return user.getPassword().equals(loginBo.getPassword());
    }

}
