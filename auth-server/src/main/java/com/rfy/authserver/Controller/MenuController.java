package com.rfy.authserver.Controller;

import com.rfy.JWT.JWTUtil;
import com.rfy.ResponseWrapper.R;
import com.rfy.authserver.Business.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/menu")
public class MenuController {

    private final MenuService menuService;

    @GetMapping("getMenu")
    public R getMenu(@RequestHeader("token") String token){
        String userId = JWTUtil.getUserId(token);
        return R.success(menuService.getMenu(userId));
    }
}
