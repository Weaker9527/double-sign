package com.rfy.authserver.Business;

import com.rfy.authserver.BO.MenuBo;
import com.rfy.authserver.DAO.mapper.MenuMapper;
import com.rfy.authserver.Model.Menu;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MenuService {

    private final MenuMapper menuMapper;

    public List<MenuBo> getMenu(String userId) {
        List<MenuBo> allMenus = menuMapper.selectMenuByUserId(userId);
        //先取出一级菜单
        List<MenuBo> parentMenus = allMenus.stream()
                .filter(item -> null == item.getParentId())
                .collect(Collectors.toList());
        parentMenus.forEach(item -> {
            item.setSon(getSon(allMenus, item.getId()));
        });
        return parentMenus;
    }

    //将菜单转换为有层级的菜单
    List<MenuBo> getSon(List<MenuBo> menus, String parentId){
        List<MenuBo> result = new ArrayList<>();
        menus.forEach(item -> {
            if(null != item.getParentId()){
                if(parentId.equals(item.getParentId())){
                    //递归
                    item.setSon(getSon(menus, item.getId()));
                    result.add(item);
                }
            }
        });
        return result;
    }
}