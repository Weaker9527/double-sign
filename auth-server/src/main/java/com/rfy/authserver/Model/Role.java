package com.rfy.authserver.Model;

import lombok.Data;

@Data
public class Role {

  private String id;
  private String name;

}
