package com.rfy.authserver.Model;

import lombok.Data;

@Data
public class User{
    private String id;

    private String name;

    private String password;
}
