package com.rfy.authserver.Model;

import lombok.Data;

@Data
public class RoleMenu {

  private String id;
  private String roleId;
  private String menuId;

}
