package com.rfy.authserver.Model;

import lombok.Data;

@Data
public class Menu {

  private String id;
  private String name;
  private String url;
  private String parentId;
  
}
