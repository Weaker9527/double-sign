package com.rfy.authserver.BO;

import lombok.Data;

@Data
public class LoginBo {

    String userName;

    String password;
}
