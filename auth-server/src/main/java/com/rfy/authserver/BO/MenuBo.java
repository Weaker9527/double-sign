package com.rfy.authserver.BO;

import com.rfy.authserver.Model.Menu;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class MenuBo extends Menu {

    private List<MenuBo> son;
}
