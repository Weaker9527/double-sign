package com.rfy.authserver.DAO.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rfy.authserver.Model.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
