package com.rfy.authserver.DAO.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rfy.authserver.BO.MenuBo;
import com.rfy.authserver.Model.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<MenuBo> selectMenuByUserId(String userId);

}
