package com.rfy.authserver.DAO.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rfy.authserver.Model.UserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
