# SpringCloud 版本兼容表

SpringBoot、SpringCloud、SpringCloudAlibaba版本需要兼容，
慎用最新版本，尽量使用Release版本，稳定兼容版本参考如下：

|Spring Cloud Version | Spring Cloud Alibaba Version | Spring Boot Version |
| ---- | ---- | ---- | 
|Spring Cloud 2020.0.1|2021.1|2.4.2|
|Spring Cloud Hoxton.SR9|2.2.6.RELEASE|2.3.2.RELEASE|
|Spring Cloud Greenwich.SR6|2.1.4.RELEASE|2.1.13.RELEASE|
|Spring Cloud Hoxton.SR3|2.2.1.RELEASE|2.2.5.RELEASE|
|Spring Cloud Hoxton.RELEASE|2.2.0.RELEASE|2.2.X.RELEASE|
|Spring Cloud Greenwich|2.1.2.RELEASE|2.1.X.RELEASE|
|Spring Cloud Finchley|2.0.4.RELEASE(停止维护，建议升级)|2.0.X.RELEASE|
|Spring Cloud Edgware|1.5.1.RELEASE(停止维护，建议升级)|1.5.X.RELEASE|

阿里系相关的中间件推荐版本兼容表如下：

|Spring Cloud Alibaba Version|Sentinel Version|Nacos Version|RocketMQ Version|DubboVersion|Seata Version|
|---- | ---- | ---- | ---- | ---- | ---- |
|2.2.6.RELEASE|1.8.1|1.4.2|4.4.0|2.7.8|1.3.0|
|2021.1 or 2.2.5.RELEASE or 2.1.4.RELEASE or 2.0.4.RELEASE|1.8.0|1.4.1|4.4.0|2.7.8|1.3.0|
|2.2.3.RELEASE or 2.1.3.RELEASE or 2.0.3.RELEASE|1.8.0|1.3.3|4.4.0|2.7.8|1.3.0|
|2.2.1.RELEASE or 2.1.2.RELEASE or 2.0.2.RELEASE|1.7.1|1.2.1|4.4.0|2.7.6|1.2.0|
|2.2.0.RELEASE|1.7.1|1.1.4|4.4.0|2.7.4.1|1.0.0|
|2.1.1.RELEASE or 2.0.1.RELEASE or 1.5.1.RELEASE|1.7.0|1.1.4|4.4.0|2.7.3|0.9.0|
|2.1.0.RELEASE or 2.0.0.RELEASE or 1.5.0.RELEASE|1.6.3|1.1.1|4.4.0|2.7.3|0.7.1|
