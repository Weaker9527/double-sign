package com.rfy.ResponseWrapper;

import java.io.Serializable;

public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;

    private String message;

    private T data;

    R() {
    }

    R(T data) {
        this.data = data;
    }

    R(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static <T> R<T> success(T data) {
        R<T> r = new R<T>(data);
        r.setCode(ResponseCode.SUCCESS.getCode());
        r.setMessage(ResponseCode.SUCCESS.getMessage());
        return r;
    }

    public static <T> R<T> fail(T data) {
        R<T> r = new R<T>(data);
        r.setCode(ResponseCode.FAIL.getCode());
        r.setMessage(ResponseCode.FAIL.getMessage());
        return r;
    }

    public static <T> R<T> fail() {
        R<T> r = new R<T>();
        r.setCode(ResponseCode.FAIL.getCode());
        r.setMessage(ResponseCode.FAIL.getMessage());
        return r;
    }

    public static <T> R<T> error(){
        R<T> r = new R<T>();
        return r;
    }

    public Integer getCode() {
        return code;
    }

    public R<T> setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public R<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}