package com.rfy.JWT;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.rfy.Exception.TokenAuthenticationException;
import com.rfy.ResponseWrapper.ResponseCode;

import java.util.Date;

/**
 * The type Jwt util.
 */
public class JWTUtil {

    //token过期时间两小时
    public static final long TOKEN_EXPIRE_TIME = 2 * 60 * 60 * 1000;
    //token发行人
    public static final String ISSUER = "RongFangyan";

    /**
     * 生成token
     *
     * @param userName    使用用户名作为用户标识
     * @param secretKey 秘钥
     * @return token
     */
    public static String generateToken(String userName, String userId, String secretKey) {
        Algorithm algorithm = Algorithm.HMAC256(secretKey);
        Date now = new Date();
        Date expireTime = new Date(now.getTime() + TOKEN_EXPIRE_TIME);

        return JWT.create()
                .withIssuer(ISSUER)
                .withIssuedAt(now)
                .withExpiresAt(expireTime)
                .withClaim("userName", userName)
                .withClaim("userId", userId)
                .sign(algorithm);
    }


    /**
     * 校验toke，校验失败抛出TokenAuthenticationException异常
     *
     * @param token token
     * @param secretKey 秘钥
     */
    public static void verifyToken(String token, String secretKey){
        try{
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier jwtVerifier = JWT.require(algorithm).withIssuer(ISSUER).build();
            jwtVerifier.verify(token);
        } catch (JWTDecodeException jwtDecodeException) {
            throw new TokenAuthenticationException(ResponseCode.TOKEN_INVALID.getCode(), ResponseCode.TOKEN_INVALID.getMessage());
        } catch (SignatureVerificationException signatureVerificationException) {
            throw new TokenAuthenticationException(ResponseCode.TOKEN_SIGNATURE_INVALID.getCode(), ResponseCode.TOKEN_SIGNATURE_INVALID.getMessage());
        } catch (TokenExpiredException tokenExpiredException) {
            throw new TokenAuthenticationException(ResponseCode.TOKEN_EXPIRED.getCode(), ResponseCode.TOKEN_INVALID.getMessage());
        } catch (Exception ex) {
            throw new TokenAuthenticationException(ResponseCode.UNKNOWN_ERROR.getCode(), ResponseCode.UNKNOWN_ERROR.getMessage());
        }
    }

    /**
     * 从token中提取出userName
     *
     * @param token token
     * @return userName 用户ID
     */
    public static String getUserName(String token){
        DecodedJWT decodedJWT = JWT.decode(token);
        return decodedJWT.getClaim("userName").asString();
    }

    public static String getUserId(String token){
        DecodedJWT decodedJWT = JWT.decode(token);
        return decodedJWT.getClaim("userId").asString();
    }

}
