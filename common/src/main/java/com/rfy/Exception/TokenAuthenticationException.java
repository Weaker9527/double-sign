package com.rfy.Exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TokenAuthenticationException extends RuntimeException {

    private int code;

    private String message;

    public TokenAuthenticationException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
