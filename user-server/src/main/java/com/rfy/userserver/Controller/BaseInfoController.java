package com.rfy.userserver.Controller;

import com.rfy.JWT.JWTUtil;
import com.rfy.ResponseWrapper.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Base info controller.
 */
@RestController
@RequestMapping("/baseInfo")
public class BaseInfoController {

    @GetMapping("/test")
    public R test(@RequestHeader("userName") String userName) {
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("userName", userName);
        return R.success(resultMap);
    }

    @GetMapping("/token")
    public R token() {
        String token = JWTUtil.generateToken("1", "1","1212");
        JWTUtil.verifyToken("1", "11");
        return R.success(token);
    }
}
