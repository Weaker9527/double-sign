package com.rfy.userserver.Controller;

import com.rfy.ResponseWrapper.R;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


/**
 * The type Test controller.
 */
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

    private final Environment environment;

    @GetMapping("/port")
    public R test() {
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("port", environment.getProperty("local.server.port"));
        return R.success(resultMap);
    }
}
