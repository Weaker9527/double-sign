package com.rfy.userserver.Config;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableOpenApi
public class SwaggerConfig {

    @Value("${spring.application.name}")
    private String self;

    @Bean
    public Docket createRestApi() {
        String packageName = "com.rfy." + self + "server.Controller";
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.basePackage(packageName))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(tokenAuth())
                .securityContexts(securityContexts())
                .apiInfo(apiInfo());
    }

    //为swagger添加token鉴权
    private static List<SecurityScheme> tokenAuth() {
        List<SecurityScheme> arrayList = new ArrayList<>();
        arrayList.add(new ApiKey("token", "token", In.HEADER.toValue()));
        return arrayList;
    }

    //授权信息全局应用
    private List<SecurityContext> securityContexts() {
        return Collections.singletonList(
                SecurityContext.builder()
                        .securityReferences(Collections.singletonList(new SecurityReference("token", new AuthorizationScope[]{new AuthorizationScope("global", "token授权")})))
                        .build()
        );
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(self+"服务API")
                .description(self+"服务管理文档API")
                .version("1.0")
                .build();
    }
}
