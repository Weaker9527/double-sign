package com.rfy.gatewayserver.Properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "filter")
public class WhiteListProp {

    private List<String> allowPaths;
}
