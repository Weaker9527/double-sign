package com.rfy.gatewayserver.Filter;

import com.google.gson.Gson;
import com.rfy.Exception.TokenAuthenticationException;
import com.rfy.JWT.JWTUtil;
import com.rfy.ResponseWrapper.R;
import com.rfy.ResponseWrapper.ResponseCode;
import com.rfy.gatewayserver.Properties.WhiteListProp;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class AuthorizeFilter implements GlobalFilter, Ordered {

    @Value("${JWT.secretKey:123456}")
    private String secretKey;

    private final int order;

    @Autowired
    private WhiteListProp whitelist;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        ServerHttpResponse serverHttpResponse = exchange.getResponse();
        String path = serverHttpRequest.getURI().getPath();

        //检查白名单
        if(isAllowed(path)){
            return chain.filter(exchange);
        }

        String token = serverHttpRequest.getHeaders().getFirst("token");
        if (StringUtils.isBlank(token)) {
            serverHttpResponse.setStatusCode(HttpStatus.UNAUTHORIZED);
            return getVoidMono(serverHttpResponse, ResponseCode.TOKEN_MISSION);
        }
        try {
            JWTUtil.verifyToken(token, secretKey);
        } catch (TokenAuthenticationException ex) {
            return getVoidMono(serverHttpResponse, ResponseCode.TOKEN_INVALID);
        } catch (Exception ex) {
            return getVoidMono(serverHttpResponse, ResponseCode.UNKNOWN_ERROR);
        }

        String userName = JWTUtil.getUserName(token);

        ServerHttpRequest mutableReq = serverHttpRequest.mutate().header("userName", userName).build();
        ServerWebExchange mutableExchange = exchange.mutate().request(mutableReq).build();

        return chain.filter(mutableExchange);
    }

    private Mono<Void> getVoidMono(ServerHttpResponse serverHttpResponse, ResponseCode codeEnum) {
        serverHttpResponse.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        R responseResult = R.error().setCode(codeEnum.getCode()).setMessage(codeEnum.getMessage());
        Gson gson = new Gson();
        DataBuffer dataBuffer = serverHttpResponse.bufferFactory().wrap(gson.toJson(responseResult).getBytes());
        return serverHttpResponse.writeWith(Flux.just(dataBuffer));
    }

    private boolean isAllowed(String path){
        //遍历配置项里面的白名单
        for(String allowPath : whitelist.getAllowPaths()){
            if(path.contains(allowPath)){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getOrder(){
        return this.order;
    }

}
