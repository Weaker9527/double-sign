package com.rfy.gatewayserver.Config;

import com.rfy.gatewayserver.Filter.AuthorizeFilter;
import com.rfy.gatewayserver.Properties.WhiteListProp;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class FilterConfig {

    @Bean
    public AuthorizeFilter authorizeFilter(){
        return new AuthorizeFilter(0);
    }

}
